package org.testleaf.leaftaps.utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class ReadExcel {
	@Test
	public static String[][] readExcelData(String wBookName) throws IOException {
	XSSFWorkbook wBook=new XSSFWorkbook("./data/"+wBookName+".xlsx");
    //XSSFSheet sheet = wBook.getSheet("data");
	XSSFSheet sheet = wBook.getSheetAt(0);
    int rowCount = sheet.getLastRowNum();
    int cellCount = sheet.getRow(0).getLastCellNum();
    String[][] data=new String[rowCount][cellCount];
    for(int i=1;i<=rowCount;i++) {
    	XSSFRow row = sheet.getRow(i);
    	for(int j=0;j<cellCount;j++) {
    		XSSFCell cell = row.getCell(j);
    		 data[i-1][j] = cell.getStringCellValue();
             System.out.println(data);
    	}
    }
    wBook.close();
    return data;
}

	
}
