package org.testleaf.leaftaps.seleniumbase;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testleaf.leaftaps.utils.ReadExcel;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;


public class ProjectSpecificMethod {
	public String excelFileName;
    public ChromeDriver driver;
    @Parameters({"url","userName","password"})
    @BeforeMethod
	public void login(String url,String uName,String pword) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		driver=new ChromeDriver();
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys(uName);
		driver.findElementById("password").sendKeys(pword);
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();

	}
    @DataProvider(name="fetchData")
    public  String[][] getData() throws IOException {
    	return ReadExcel.readExcelData(excelFileName);
    }
    @AfterMethod
    public void closeBrowser() {
    	driver.close();
    }
    @BeforeClass
    public void beforeClass() {
    	System.out.println("Before Class Anotation executed");
    }
    @AfterClass
    public void afterClass() {
    	System.out.println("After Class Anotation executed");
    }
    @BeforeTest
    public void beforeTest() {
    	System.out.println("Before test Anotation executed");
    }
    @AfterTest
    public void afterTest() {
    	System.out.println("After Test Anotation executed");
    }
    @BeforeSuite
    public void beforeSuite() {
    	System.out.println("Before Suite Anotation executed");
    }
    @AfterSuite
    public void afterSuite() {
    	System.out.println("After Suite Anotation executed");
    }
}
