package org.testleaf.leaftaps.leads.testcases;

import org.testleaf.leaftaps.seleniumbase.ProjectSpecificMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CreateLead extends ProjectSpecificMethod{
	@BeforeClass
	public void setData() {
		excelFileName="CreateLead";
	}
	@Test(dataProvider="fetchData")
	public void runCreateLead(String cName,String fName,String lName) {
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys(cName);
		driver.findElementById("createLeadForm_firstName").sendKeys(fName);
		driver.findElementById("createLeadForm_lastName").sendKeys(lName);
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("74");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("8939350809");
		driver.findElementByName("submitButton").click();
}
//	@DataProvider(name="fetchData")
//	public static String[][] getData() {
//		String[][] data=new String[2][3];
//		data[0][0]="Nihon Technology Private Limited";
//		data[0][1]="Karthik";
//		data[0][2]="M";
//	
//		data[1][0]="CTS";
//		data[1][1]="Ganesh";
//		data[1][2]="V";

//		return getData();
//		
//	}

}
