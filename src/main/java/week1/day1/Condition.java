package week1.day1;

public class Condition {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a=15;
		int b=30;
		int c=230;
		//Checking two condition in same statement is another logic
		if(a>b) {
			System.out.println(a+"is greater number");
		}
		else if(b>c){
			System.out.println(b+"is greater number");
		}
		else {
			System.out.println(c+"is greater number");
		}
	}

}
