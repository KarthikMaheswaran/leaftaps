package week3.day6;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class IRMASTPJ {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://erail.in/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(2000,TimeUnit.SECONDS);
		driver.findElementById("txtStationFrom").clear();  
		driver.findElementById("txtStationFrom").sendKeys("Ms",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("TPJ",Keys.TAB);
		WebElement ele = driver.findElementByXPath("(//table/tbody/tr/td/input)[1]");
		if(ele.isSelected()) 
		{
			ele.click();
		}
		List<WebElement> trainnumber = driver.findElementsByXPath("//table/tbody/tr/td/a[contains(@title,'train number ')]");
		trainnumber.size();
		List<String> list1=new ArrayList<String>();
		for(WebElement c:trainnumber)
		{
			list1.add(c.getText());
		}
		Collections.sort(list1);
		System.out.println(list1);
		driver.findElementByXPath("//table/tbody/tr/td/a[text()='Train']").click();
		List<WebElement> trainnumber1 = driver.findElementsByXPath("//table/tbody/tr/td/a[contains(@title,'train number ')]");
		List<String> list2=new ArrayList<>();
		for(WebElement b:trainnumber1)
		{
			list2.add(b.getText());
		}
		System.out.println(list2);
		if(list1.equals(list2))
		{
			System.out.println("sorted properly");
		}
		Set<String> a=new HashSet<String>();
		Set<String> d=new HashSet<String>();
		for(String i:list1)
		{
			if(!a.add(i))
			{
				d.add(i);
			}
		}
		System.out.println(d);

		driver.close();


	}

}
