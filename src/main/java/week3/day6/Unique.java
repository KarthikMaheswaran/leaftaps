package week3.day6;

import java.util.LinkedHashMap;
import java.util.Map;

public class Unique {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str="PayPal India";
		char[] cs = str.toCharArray();
		System.out.println(cs);
		/*String[] split = str.split(" ");
		int len = split.length;
		System.out.println(len);
*/
		LinkedHashMap<Character, Integer> list = new LinkedHashMap<Character, Integer>();
		for(Character c : cs)
		{  
			if(list.containsKey(c))
			{
				list.put(c, list.get(c)+1);
			}
			else
			{
				list.put(c, 1);
			}
		}
		
		System.out.println(list);
		for(@SuppressWarnings("rawtypes") Map.Entry e: list.entrySet())
		{
			if((int)e.getValue() == 1)
				System.out.print(e.getKey());
			
		}
		
	}

}
