package week3.day6;

import java.util.HashMap;
import java.util.Map;

public class Infosys {

	public static void main(String[] args) {
		// TODO Auto-generated method stub 
		String companyname="Infosys Limited";
		char[] ca = companyname.toCharArray();
        Map<Character,Integer> map=new HashMap<>();
        for(char c:ca) {
        	if(map.containsKey(c)) {
        		map.put(c,map.get(c)+1);
        		System.out.println(map);
        		
        	}else {
        		map.put(c, 1);
        	}
        }
        System.out.println(map);
	}

}
