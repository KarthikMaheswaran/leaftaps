package week3.day6;

import java.util.HashMap;
import java.util.Map;


public class LearnMap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String companyname="Nihon Technology Private Limited";
		char[] companychar = companyname.toCharArray();

		System.out.println(companychar);
		Map<Character,Integer> map=new HashMap<>();
		for (char c : companychar) {
			if (map.containsKey(c)) {
				map.put(c, map.get(c)+1);
			}else {
				map.put(c, 1);
			}

		}
		System.out.println(map);

	}

}
