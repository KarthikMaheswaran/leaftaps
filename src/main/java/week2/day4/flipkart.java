package week2.day4;

import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class flipkart {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//set The Driver Path
				System.setProperty("webdriver.chrome.driver",
						"./drivers/chromedriver.exe");
				//launch chrome
				ChromeDriver driver = new ChromeDriver();
				// implicit wait
				driver.manage().timeouts().implicitlyWait
				(30, TimeUnit.SECONDS);
				//maximize the Browser 
				driver.manage().window().maximize();
				//login
				driver.get("https://www.flipkart.com");
				driver.findElementByXPath("//button[text()='✕']").click();
				WebElement electronics = driver.findElementByXPath("//span[text()='Electronics']");
				
				// AUI
				Actions builder = new Actions(driver);
				builder.moveToElement(electronics).perform();
				WebDriverWait wait = new WebDriverWait(driver, 10);
				WebElement mi = driver.findElementByLinkText("Mi");
				wait.until(ExpectedConditions.visibilityOf(mi));
				mi.click();
				String text1 = driver.findElementByXPath("//h1[text()='Mi Mobiles']").getText();
				if(text1.equals("Mi Mobiles")) {
					System.out.println("Title Mi Mobiles is present");
				}
				else
				{
					System.out.println("Mi Mobile title is not present");
				}
				String model = driver.findElementByXPath("//div[text()='Redmi Note 7 Pro (Space Black, 64 GB)']").getText();
				String price = driver.findElementByXPath("(//div[text()='₹13,999'])[1]").getText();
				System.out.println(model +  price);
	}

}
