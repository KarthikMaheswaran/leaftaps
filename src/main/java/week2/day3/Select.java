package week2.day3;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class Select {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();
		driver.get("http://www.leafground.com/pages/checkbox.html");
		//driver.get("http://www.leafground.com/pages/Link.html");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println(driver.findElementByXPath("//*[@id=\"contentblock\"]/section/div[1]/input[1]").isSelected());
		System.out.println(driver.findElementByXPath("//*[@id=\"contentblock\"]/section/div[2]/input").isSelected());
		driver.close();
		
	}

}
