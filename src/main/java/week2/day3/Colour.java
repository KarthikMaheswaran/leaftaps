package week2.day3;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class Colour {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();
		driver.get("http://www.leafground.com/pages/Button.html");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//getCSSValue
		System.out.println(driver.findElementById("color").getCssValue("background-color"));
		//getText
		System.out.println(driver.findElementById("color").getText());
		//getLocation
		System.out.println(driver.findElementById("color").getLocation().getX());
		System.out.println(driver.findElementById("color").getLocation());
		System.out.println(driver.findElementById("color").getSize());
		System.out.println(driver.findElementById("color").getSize().getHeight());
		System.out.println(driver.findElementById("color").getTagName());
		System.out.println(driver.findElementById("color").isDisplayed());
		driver.close();
	}

}
