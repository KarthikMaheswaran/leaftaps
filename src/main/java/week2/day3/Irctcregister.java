package week2.day3;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Irctcregister {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("userRegistrationForm:userName").sendKeys("Karthik");
		driver.findElementById("userRegistrationForm:password").sendKeys("Karthik123");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("Karthik123");
		WebElement sourceele = driver.findElementById("userRegistrationForm:securityQ");
		Select source= new Select(sourceele);
		source.selectByVisibleText("What is your pet name?");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Puppy");
		WebElement sourceele1 = driver.findElementById("userRegistrationForm:prelan");
		Select source1= new Select(sourceele1);
		source1.selectByVisibleText("English");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Karthik");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("Maheswaran");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Perumal");
		driver.findElementById("userRegistrationForm:gender:0").click();
		driver.findElementById("userRegistrationForm:maritalStatus:1").click();
		WebElement days = driver.findElementById("userRegistrationForm:dobDay");
		Select day=new Select(days);
		day.selectByVisibleText("02");
		WebElement months = driver.findElementById("userRegistrationForm:dobMonth");
		Select month=new Select(months);
		month.selectByVisibleText("DEC");
		WebElement years = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select year=new Select(years);
		year.selectByVisibleText("1993");
		//driver.findElementById("userRegistrationForm:occupation").sendKeys("");
		WebElement occupations = driver.findElementById("userRegistrationForm:occupation");
		Select occupation=new Select(occupations);
		occupation.selectByVisibleText("Professional");
		driver.findElementById("userRegistrationForm:uidno").sendKeys("741683229417");
		driver.findElementById("userRegistrationForm:idno").sendKeys("CNTMPL440L");
		WebElement countries = driver.findElementById("userRegistrationForm:countries");
		Select country=new Select(countries);
		country.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:email").sendKeys("karthik@gmail.com");
		//driver.findElementById("userRegistrationForm:isdCode").sendKeys("+91");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("8939350809");
		WebElement nations = driver.findElementById("userRegistrationForm:nationalityId");
		Select nation=new Select(nations);
		nation.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:address").sendKeys("No.4/215");
		driver.findElementById("userRegistrationForm:street").sendKeys("Erattaimalai srinivasanst");
		driver.findElementById("userRegistrationForm:area").sendKeys("Kalliamman Ng,Pozichalur");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600074",Keys.TAB);
		Thread.sleep(8000);
		WebElement cities = driver.findElementById("userRegistrationForm:cityName");
		Select city=new Select(cities);
		city.selectByVisibleText("Kanchipuram");
		Thread.sleep(8000);
		WebElement towns = driver.findElementById("userRegistrationForm:postofficeName");
		Select town=new Select(towns);
		town.selectByVisibleText("Polichalur S.O");
		driver.findElementById("userRegistrationForm:landline").sendKeys("8939350809");
		driver.findElementById("userRegistrationForm:resAndOff:0").click();
	}

}
