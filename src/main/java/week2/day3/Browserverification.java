package week2.day3;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class Browserverification {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();
		driver.get("http://www.leafground.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		//Browser Verification using getTitle
		System.out.println("The title of the page is:"+driver.getTitle());
		if(driver.getTitle().contains("TestLeaf")) {
			System.out.println("I confirmed the title has testleaf");
		}else
		{
			System.out.println("The title doesn't contains testleaf");
		}
		
		//Browser Verification using getCurrentURL
		System.out.println("The Current URL is:"+driver.getCurrentUrl());
		
		//get PageSource
		System.out.println("The Page Source is:"+driver.getPageSource());
		driver.close();
	}

}
