package week2.day3;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Nihon Technology");
		driver.findElementById("createLeadForm_firstName").sendKeys("Karthik");
		driver.findElementById("createLeadForm_lastName").sendKeys("M");
		//driver.findElementByName("submitButton").click();
		
		WebElement sourceele = driver.findElementById("createLeadForm_dataSourceId");
		Select source= new Select(sourceele);
		source.selectByVisibleText("Employee");
		
//		WebElement ele = driver.findElementById("createLeadForm_marketingCampaignId");
//		Select source1=new Select(ele);
//		source1.selectByValue("9001");
		
		
		//List<WebElement> ele1 = driver.findElementsById("createLeadForm_marketingCampaignId");
		//int size = ele1.size();
		WebElement webElement1 =driver.findElementById("createLeadForm_marketingCampaignId");;
		Select source2= new Select(webElement1);
		List<WebElement> options = source2.getOptions();
		source2.selectByIndex(options.size()-2);
		
		
		//driver.close();

	}

}
