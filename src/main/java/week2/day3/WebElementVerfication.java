package week2.day3;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class WebElementVerfication {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();
		driver.get("http://www.leafground.com/pages/Edit.html");
		//driver.get("http://www.leafground.com/pages/Link.html");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		//getAttributes
		System.out.println(driver.findElementByName("username").getAttribute("value"));
		//get hyperlink value
		//System.out.println(driver.findElementByLinkText("Find where am supposed to go without clicking me?").getAttribute("href"));
		System.out.println(driver.findElementByXPath("//*[@id=\"contentblock\"]/section/div[5]/div/div/input").isEnabled());
		driver.close();
	}

}
