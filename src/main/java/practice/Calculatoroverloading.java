package practice;

public class Calculatoroverloading {
	void sum(int a,int b) {
		System.out.println(a+b);
	}
	void sum(int a,int b,int c) {
		System.out.println(a+b+c);
	}
	void sum(double a,double b) {
		System.out.println(a+b);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Calculatoroverloading c=new Calculatoroverloading();
		c.sum(10, 20);
		c.sum(5, 5, 5);
		c.sum(10.5, 5.5);
	}

}
