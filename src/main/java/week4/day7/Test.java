package week4.day7;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;

import org.openqa.selenium.chrome.ChromeDriver;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[@onclick=\"myFunction()\"]").click();
		Alert alert = driver.switchTo().alert();
		alert.sendKeys("Karthik");
		alert.accept();
		String text = driver.findElementByXPath("//*[@id=\"demo\"]").getText();
		if(text.contains("Karthik")) {
			System.out.println("Successful");
		}
		if(text.equals("Hello Karthik! How are you today?")) {
			System.out.println("String displayed successfully");
		}
			driver.close();	
	}

}
