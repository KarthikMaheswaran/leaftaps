package week4.day7;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class Learnwindows {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
        driver.findElementByLinkText("Contact Us").click();
        Set<String> allwindows = driver.getWindowHandles();
        //get window return set of string 
        List<String> listwindow=new ArrayList<String>(allwindows);
        driver.switchTo().window(listwindow.get(1));
        System.out.println(driver.getTitle());
        File src = driver.getScreenshotAs(OutputType.FILE);
        File target=new File("./snaps/Img001.jpg");
        FileUtils.copyFile(src,target);
        driver.close();
        //driver.switchTo().defaultContent();
        //driver.close();
	}

}
