package week4.day8;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Facebook {

	public static void main(String[] args) throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		ChromeOptions ops = new ChromeOptions();
		ops.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(ops);
		driver.get("https://www.facebook.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.findElementById("email").clear();
		driver.findElementById("email").sendKeys("8939350809");
		driver.findElementById("pass").clear();
		driver.findElementById("pass").sendKeys("karthik@0212");
		driver.findElementByXPath("//input[@type='submit']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//input[@type='text'][2]").sendKeys("TestLeaf");
		driver.findElementByXPath("//button[@type='submit'][1]").click();
//		driver.findElementByXPath("//a[text()='TestLeaf']").click();
//        boolean enabled = driver.findElementByXPath("(//button[@type='submit'])[3]").isSelected();
//        if(enabled==true) {
//			System.out.println("Test Leaf like button is already selected");
//		
//	}
		Thread.sleep(3000);
		String text = driver.findElementByXPath("(//button[@type='submit'])[2]").getText();
		if(text.equals("Liked")) {
			System.out.println("Already Liked");
		}
		else {
			driver.findElementByXPath("(//button[@type='submit'])[2]").click();
//			driver.findElementByXPath("(//button[@type='submit'])[2]").getText();
			System.out.println(driver.findElementByXPath("(//button[@type='submit'])[2]").getText());
			
		}
		driver.findElementByXPath("//a[text()='TestLeaf']").click();
		System.out.println(driver.getTitle().contains("TestLeaf"));
		File src = driver.getScreenshotAs(OutputType.FILE);
		File target=new File("./snaps/Img001.jpg");
	    FileUtils.copyFile(src,target);
	}
	}

