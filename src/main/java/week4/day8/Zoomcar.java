package week4.day8;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Zoomcar  {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.zoomcar.com/chennai");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.findElementByXPath("//*[@id=\"home\"]/div[3]/div/div[2]/a").click();
		driver.findElementByXPath("/html/body/div[1]/div/div[2]/div[1]/div[2]/div[4]/div/div[3]").click();
		driver.findElementByXPath("/html/body/div[1]/div/div[3]/div[2]/button").click();
		LocalDate now=LocalDate.now();
		//System.out.println("now.getDayOfMonth()+1");
		int d=now.getDayOfMonth()+1;
		driver.findElementByXPath("//div[contains(text(),'"+d+"')]").click();
		driver.findElementByXPath("//button[text()='Next']").click();
		boolean pick = driver.findElementByXPath("//div[@class='day picked ']").isDisplayed();
		System.out.println(pick);
		driver.findElementByXPath("//button[text()='Done']").click();
//		List<WebElement> list = driver.findElements(By.className("car-listing"));
//		System.out.println(list.size());
//		for(int i=0;i<list.size();i++) {
//			
		Thread.sleep(2000);
		List<WebElement> carNames = driver.findElementsByTagName("h3");
		List<WebElement> prices = driver.findElementsByXPath("//div[@class='price']");
		Map<String,Integer> map= new HashMap<>();
		for(int i=0;i<prices.size();i++) {
			String replaceAll = prices.get(i).getText().replaceAll("\\D", "");
			int parseInt = Integer.parseInt(replaceAll);
			map.put(carNames.get(i).getText(),parseInt);
			}
			System.out.println(map);
			int max = 0;
			for (Entry<String, Integer>  values : map.entrySet()) {
			if (values.getValue() > max) {
			max = values.getValue();
			}
			}
			for (Entry<String, Integer>  CarName : map.entrySet()) {
			if(CarName.getValue() == max) {
			System.out.println("car name: "+CarName.getKey()+" price: "+max);
			}
		}
		
		}
		}	


