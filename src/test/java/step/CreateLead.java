package step;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {
	public ChromeDriver driver;
	@Given("Launch the Browser")
	public void launchTheBrowser() {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		driver=new ChromeDriver();
	    
	}

	@And("Maximize the Browser")
	public void maximizeTheBrowser() {
		driver.manage().window().maximize();
	}

	@And("set the Timeouts")
	public void setTheTimeouts() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@And("Load the URL")
	public void loadTheURL() {
		driver.get("http://leaftaps.com/opentaps");
	}

	@And("Enter the Username as (.*)")
	public void enterTheUsernameAsDemoSalesManager(String data) {
		driver.findElementById("username").sendKeys(data);
	}

	@And("Enter the Password as (.*)")
	public void enterThePasswordAsCrmsfa(String data) {
		driver.findElementById("password").sendKeys(data); 
	}

	@And("I click on the Login Button as decorativeSubmit")
	public void iClickOnTheLoginButtonAsDecorativeSubmit() {
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@And("Verify Login is Success")
	public void verifyLoginIsSuccess() {
	    System.out.println("Logged in Successfully");
	    driver.findElementByLinkText("CRM/SFA").click();
	}
	
	@And("click leads")
	public void clickLead() {
		driver.findElementByLinkText("Leads").click();
	}
	@And("click createLead")
	public void clickCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
	}
	@And("Enter companyname as (.*)")
	public void companyName(String data) {
		driver.findElementById("createLeadForm_companyName").sendKeys(data);
	}
	@And("Enter Firstname as (.*)")
	public void firstName(String fname) {
		driver.findElementById("createLeadForm_firstName").sendKeys(fname);
	}
	@And("Enter Lastname as (.*)")
	public void lastName(String lname) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lname);
	}
	@When("Click Submit button")
	public void clickButton(){
		driver.findElementByName("submitButton").click();
	}
	@Then("Lead created successfully")
	public void leadSucess() {
		String text1 = driver.findElementById("viewLead_companyName_sp").getText();
		String replaceAll1 = text1.replaceAll("\\D", "");
		System.out.println(replaceAll1);
		System.out.println("Lead created successfully");
		driver.close();
	}
}
