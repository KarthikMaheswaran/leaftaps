Feature: Leaftaps Login

Background:
Given Launch the Browser
And Maximize the Browser
And set the Timeouts
And Load the URL


Scenario Outline: TC001 Positive Login Flag
Given Enter the Username as <uName> 
And Enter the Password as <password>
And I click on the Login Button as decorativeSubmit
And Verify Login is Success
And click leads 
And click createLead
And Enter companyname as <cName>
And Enter Firstname as <fName>
And Enter Lastname as <lName>
When Click Submit button
Then Lead created successfully

Examples:
|uName|password|cName|fName|lName|
|DemoSalesManager|crmsfa|Nihon Technology Private Limited|Karthik|Maheswaran|
|DemoSalesManager|crmsfa|CTS|Ganeshk|Velyadum|
